#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Author : Brunus
Licence : MIT
Version : 0.1.2
'''

import sys
import os
import argparse
import subprocess

mutagen_missing_error = "uncompiler needs mutagen library to be installed : https://mutagen.readthedocs.io/en/latest/"

# Try to import mutagen lib
try:
    from mutagen.easyid3 import EasyID3
except:
    print(mutagen_missing_error)
    sys.exit()

# Parsing command line
parser = argparse.ArgumentParser(
    description='Soundcut splits full albums music files or compilation in several music files, using a playlist and timecode.')
parser.add_argument('-i', '--inputfile', help='music filename', required=True)
parser.add_argument('-p', '--playlist', help='playlist to use', required=True)
parser.add_argument('-A', '--artist', help='artist name', required=False)
parser.add_argument('-a', '--album', help='album name', required=False)
parser.add_argument('-g', '--genre', help='genre', required=False)
parser.add_argument('-y', '--year', help='year', required=False)

args = parser.parse_args()

if __name__ == "__main__":

    file_playlist = args.playlist
    file_sound = args.inputfile
    if args.artist :
        artist = args.artist


        # Underscore prefixed variables are there to create a filename for the ffmpeg command line
        # We need this filname to be free from spaces and special characters
        # Not prefixed variables are there for the final filename and tags

        _artist = artist.replace(" ", "_")
        _artist = ''.join(e for e in artist if e.isalnum() and e != '_')

    # loading the playlist
    with open(file_playlist, "r") as playlist:
        datas = playlist.readlines()

        # Striping \n chars in lines
        datas = [x.strip() for x in datas]

        # for each track we need to extract : timecodes for start time and end time, artist name, track's title
        for i in range(len(datas)):

            # The first line is the timecode for the end of the last track
            if i == 0:
                print(datas[i])
                duration = datas[i].split(' ')[1]
                print("Duration : " + duration)
                print(" ")

            # Parsing tracks
            else :
                currentsong = datas[i].split(' - ')

                start = currentsong[0]
                print(start)

                if args.artist :
                    title = currentsong[1]
                    print("Current song : " + artist + ' - ' + title)
                    print(" ")

                else :
                    artist = currentsong[1]
                    title = currentsong[2]
                    print(" ")
                    print("Current song : " + artist + ' - ' + title)

                    _artist = artist.replace(" ", "_")
                    _artist = ''.join(e for e in artist if e.isalnum() and e != '_')

                _title = title.replace(" ", "_")
                _title = ''.join(e for e in title if e.isalnum() and e != '_')

                # Building filenames for temporary file and final one
                if args.artist :
                    track = str(i).zfill(2)
                    _output_file = track + "_" + _artist + "_" + _title + ".mp3"
                    output_file = track + " - " + artist + " - " + title + ".mp3"

                else :
                    _output_file = _artist + "_" + _title + ".mp3"
                    output_file = artist + " - " + title + ".mp3"

                # Adding zeros to timecode if needed
                if (i < len(datas)-1):

                    if (len(start) < 6 and len(start) < 5) :
                            start = start.zfill(5)

                    if len(start) > 5 and len(start) < 8:
                            start = start.zfill(8)

                    nextsong = datas[i+1].split(' - ')
                    end = nextsong[0]

                    if (len(end) < 6 and len(end) < 5) :
                            end = end.zfill(5)

                    if len(end) > 5 and len(end) < 8:
                            end = end.zfill(8)
                else :
                    if (len(start) < 6 and len(start) < 5) :
                            start = start.zfill(5)

                    if len(start) > 5 and len(start) < 8:
                            start = start.zfill(8)
                    end = duration

                # Building and running ffmpeg command line to extract the current track
                # print ("input file" + ' ' + file_sound)
                print ("output file" + ' ' + output_file)
                print ("start" + ' ' + start)
                print ("end" + ' ' + end)

                command_line = "ffmpeg -hide_banner -loglevel panic -nostats -i " + "\"" +file_sound + "\"" + " -acodec copy -ss " + start + " -to " + end + " " + _output_file
                print (command_line)
                subprocess.call(command_line, shell=True)

                # taging with mutagen
                audiofile = EasyID3(_output_file)

                if args.artist:
                    audiofile['artist'] = str(args.artist)
                    audiofile['tracknumber'] = str(i).zfill(2)
                else:
                    audiofile['artist'] = str(artist)

                audiofile['title'] = str(title)

                if args.album:
                    audiofile['album'] = str(args.album)

                if args.genre:
                    audiofile['genre'] = str(args.genre)

                if args.year:
                    audiofile['date'] = str(args.year)

                audiofile.save()

                # Renaming temporary file to give it full unicode filename
                os.rename(_output_file, output_file)
