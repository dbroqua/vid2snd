# CHANGELOG

V 0.1.2

New feature :

- Added playlist function
It's possible to add the title to a local playlist or to create a new one.

Bugs :
- Fixed a bug in the ogg filename generation.

TODO :
- Rewrite the calculate_duration() fuction bacause it's not accurate enough.
The actual calculate_duration function is based on the mutagen info extractor.
But this mutagen function is based on a VBR case, and then return values very different from what we ar waiting for.


v 0.1.1

- bt or bitrate param added, default is 192KBs
- tn or tracknumber param added, not required

Notes : The control on track number param's consictency could be improved or more pythonic

